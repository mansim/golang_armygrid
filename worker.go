package main

import (
"fmt"
"github.com/Scalingo/go-workers"
)

func main() {
workers.Configure(map[string]string{
    "process": "worker1", "server": "localhost:6379", "namespace": "goworkers",
}) 
workers.Process("myqueue", CalculateFight, 10)
workers.Run()
}

type UnitStats struct {
    dmg int
    def_inf int
    def_hors int
    def_arch int
    speed int
    unit_type string
    unit_name string
}

// unit_types: 1 = 'inf', 2: 'horse', 3: 'arch', unit_names: 1 = 'sword', 1 = 'sword'
var unit_stats = map[int]map[string]int{
    1: map[string]int{
      "dmg": 25, 
      "def_inf": 55, 
      "def_hors": 5, 
      "def_arch": 30, 
      "speed": 30, 
      "unit_type": 1, 
      "unit_name": 1},
}

// (unit_type) teams: {1: ally, 2: enemy}. stats: {1: 'sword'} 
var formations = map[int][]map[string]int{
     1:  {map[string]int{"stats": 1, "column": 10, "row": 10, "x": 50, "y": 135, "team": 1,  "ident": 0}},
    -1: {map[string]int{"stats": 1, "column": 10, "row": 10, "x": 78, "y": 174, "team": -1, "ident": 0}},
}

var frame int = 0
var distance_x int = 0
var distance_y int = 0
var army = make(map[int]map[string]int)
var group = make(map[int]map[int]map[int]map[string]int)
var unit = make(map[string]int)
var attacking = make(map[string]int)
var target = make(map[string]int)
var stats = make(map[string]int)
var closest_y int = 0
var string_y int = 0




func search_for_target() {
    target = make(map[string]int)
    if closest_y != unit["y"] {
        // target = iterate_X();
    } else {
        // target = iterate_X_X();
    }
    if (len(target) == 0) {
        if(closest_y != unit["y"]) {
            // target = iterate_backwards_X_X();
        } else {
            // target = iterate_backwards_X();
        }
    }
}


// func iterate_backwards_X() {
//     if(army[unit.id].unit_type == 'ally')  return _.find(group.enemy[closest_y], function (v, k) { return parseInt(k) <= unit.x });
//     if(army[unit.id].unit_type == 'enemy')  return _.find(group.ally[closest_y], function (v, k) { return parseInt(k) >= unit.x });
// };

// func iterate_backwards_X_X() {
//     if(army[unit.id].unit_type == 'ally')  return _.find(group.enemy[closest_y], function (v, k) { return parseInt(k) <= unit.x });
//     if(army[unit.id].unit_type == 'enemy')  return _.find(group.ally[closest_y], function (v, k) { return parseInt(k) >= unit.x });
// };

// func iterate_X() { 
//     if(army[unit.id].unit_type == 'ally')  return _.find(group.enemy[closest_y], function (v, k) { return parseInt(k) >= unit.x });
//     if(army[unit.id].unit_type == 'enemy')  return _.find(group.ally[closest_y], function (v, k) { return parseInt(k) <= unit.x });
// };

// func iterate_X_X() {
//     if(army[unit.id].unit_type == 'enemy') return _.find(group.ally[closest_y],  function (v, k) { return parseInt(k) <= unit.x });
//     if(army[unit.id].unit_type == 'ally') return _.find(group.enemy[closest_y],  function (v, k) { return parseInt(k) >= unit.x });
// };


func calculate_units() {
    group_types := [2]int{1, -1}
    for i := 0; i < 2; i++ {
        var group_type int = group_types[i]
        for y, value := range group[group_type] { 
            var closest_y int = iterate_y(y, group_type)
            for x, unitas := range value {
                unit = unitas
                stats = unit_stats[army[unit["id"]]["stats"]]
                setAllyEnemy()
                set_attacking_and_target() 
                if (survived()) {
                  var _ = x
                  var _ = closest_y
                    unit["changed"] = 0

                    // if unit["able_to_move"] == 1 && (len(target) == 0 || frame % 500 === 0) {
                    //     search_for_target()
                    //     unit["changed"] = 1
                    // }

                //     if(unit.old_move_x != unit.move_x || unit.old_move_y != unit.move_y ) {
                //         if(unit.status != 1) {
                //             unit.changed = true;
                //             able_surroundings_to_move();
                //         }
                //         unit.status = 1;
                //     } else {
                //         if(unit.status != 0) {
                //             unit.changed = true;
                //         } 
                //         if(unit.able_to_move && !can_move_somewhere()) {
                //             unit.able_to_move = false;
                //         }
                //         unit.status = 0;
                //     }

                //     numbers[type] ++;

                //     if( (x != unit.x) || (y != unit.y) ) {
                //         delete group[type][y][x];
                //         if(_.isEmpty(group[type][y])) {
                //             delete group[type][y];
                //         }
                //         if(!_.has(group[type], unit.y)) group[type][unit.y] = {};
                //     } 

                //     if (unit.changed) {
                //         if(!_.has(changed_group[type], unit.y)) changed_group[type][unit.y] = {};
                //         changed_group[type][unit.y][unit.x] = unit;
                //     }

                //     group[type][unit.y][unit.x] = unit;
                   
                // } else {
                //     delete group[type][y][x];
                //     if(_.isEmpty(group[type][y])) {
                //         delete group[type][y];
                //     }
                }
            }
        }
    }

    //         
    //         });
    //     });
    // });
}




func survived() bool {
    if unit["hp"] < 1 {
        able_surroundings_to_move()
        return false
    }
    unit["old_move_y"] = unit["move_y"]
    unit["old_move_x"] = unit["move_x"]
    check_forward()
    return true
};

func initiate() {
    group_types := [2]int{1, -1}
    for i := 0; i < 2; i++ {
        var group_type int = group_types[i]
        group[group_type] = make(map[int]map[int]map[string]int)
        var army_id int = 0
        var ally_length int = len(formations[group_type])
        for row := 0; row < ally_length; row++ {
            army_id++
            army[army_id] = map[string]int{"stats": formations[group_type][row]["stats"]}
            var ally_row_y int = formations[group_type][row]["y"]
            var ally_row_row int = formations[group_type][row]["row"] + ally_row_y
            for y := ally_row_y; y < ally_row_row; y++ {
                elem, ok := group[group_type][y]
                var columns = make(map[int]map[string]int)
                if ok { columns = elem }
                var ally_row_x int = formations[group_type][row]["x"];
                var ally_row_column int = -formations[group_type][row]["column"] + ally_row_x;
                if group_type == 1 {
                    for x := ally_row_x; x > ally_row_column; x-- {
                        var able_to_move int = 0;
                        if y == ally_row_y || x == ally_row_x || y == ally_row_row-1 || x == ally_row_column+1 {able_to_move = 1}
                        columns[x] = return_columns_object(x, y, able_to_move, army_id, 1)
                    }
                } else {
                    for x := ally_row_x; x < ally_row_column; x++ {
                        var able_to_move int = 0;
                        if y == ally_row_y || x == ally_row_x || y == ally_row_row-1 || x == ally_row_column-1 {able_to_move = 1}
                        columns[x] = return_columns_object(x, y, able_to_move, army_id, -1)
                    }
                }
                group[group_type][y] = columns
            }
        }
    }
}


func check_forward() {
    if (unit["stopped"] == 1) {
        check_temporary_moves()
        if (len(attacking) == 0) {
            try_moving()
        } else {

            if attacking["hp"] > 0 {
                if unit["team"] == 1 {
                    attacking["hp"] -= stats["dmg"]
                } else {
                    if unit_stats[army[attacking["id"]]["stats"]]["unit_type"] == 1 {
                        attacking["hp"] -= stats["def_inf"]
                    } else if unit_stats[army[attacking["id"]]["stats"]]["unit_type"] == 2 {
                        attacking["hp"] -= stats["def_hors"]
                    } else if unit_stats[army[attacking["id"]]["stats"]]["unit_type"] == 3 {
                        attacking["hp"] -= stats["def_arch"]
                    } else {
                      fmt.Println("unexpected unit type")
                    }
                }
                if attacking["attacking_x"] == -999999999 && attacking["attacking_y"] == -999999999 {
                    attacking["attacking_x"] = unit["x"]
                    attacking["attacking_y"] = unit["y"]
                    attacking["target_x"] = unit["x"]
                    attacking["target_y"] = unit["y"]
                    attacking["able_to_move"] = 1;
                    attacking["stopped"] = 1;
                }
                if attacking["hp"] < 1 {
                    unit["killed"] += 1
                    target = make(map[string]int)
                    unit["target_x"] = -999999999
                    unit["target_y"] = -999999999
                }
            } else if len(attacking) != 0 {
                target = make(map[string]int)
                attacking = make(map[string]int)
                unit["target_x"] = -999999999
                unit["target_y"] = -999999999
                unit["attacking_x"] = -999999999
                unit["attacking_y"] = -999999999
            }
        }
    } else {
        if (!find_opponent()) {
            if(len(target) != 0 && unit["x"] == target["x"] && unit["y"] == target["y"]) {
                unit["waiting"] = 1
            } else {
                try_moving()
            }
        }
    }
}

func able_surroundings_to_move() {
    for xx := -1; xx < 2; xx+=2 {
        var new_x_1 int = unit["x"]+xx
        var new_y_1 int = unit["y"]+xx
        if len(group[unit["ally"]][unit["y"]][new_x_1]) != 0 {
            group[unit["ally"]][unit["y"]][new_x_1]["able_to_move"] = 1
        }
        if len(group[unit["ally"]][new_y_1][unit["x"]]) != 0 {
            group[unit["ally"]][new_y_1][unit["x"]]["able_to_move"] = 1
        }
    }
}

func find_opponent() bool {
    if len(target) != 0 {
        if target["y"] == unit["y"] {
            var y_array = [3]int{0, 1, -1}
            for yy := 0; yy < 3; yy++ {
                var new_y int = unit["y"] + y_array[yy]
                var new_x int = unit["x"] + unit["direction_x"]
                if len(group[unit["enemy"]][new_y][new_x]) != 0 {
                    attacking = group[unit["enemy"]][new_y][new_x]
                    target = group[unit["enemy"]][new_y][new_x]
                    unit["stopped"] = 1
                    return true
                }
            }
        } else {
            var new_y int = unit["y"] + unit["direction_y"]
            if len(group[unit["enemy"]][new_y][unit["x"]]) != 0 {
                attacking = group[unit["enemy"]][new_y][unit["x"]]
                target = group[unit["enemy"]][new_y][unit["x"]]
                unit["stopped"] = 1
                return true
            }
        }
    } else {
        var new_y int = unit["y"]
        var new_x int = unit["x"] + unit["direction_x"]
        if len(group[unit["enemy"]][new_y][new_x]) != 0 {
            attacking = group[unit["enemy"]][new_y][new_x]
            target = group[unit["enemy"]][new_y][new_x]
            unit["stopped"] = 1
            return true
        }
    }
    return false
}


func check_temporary_moves() {
    if(unit["direction_y"] == 1 && unit["move_y"] < 99) {
        if(len(attacking) != 0 && attacking["move_y"] > unit["move_y"] + 1) {
            unit["move_y"] += stats["speed"]
        } else if (len(attacking) == 0 && unit["move_y"] < 99) {
            unit["move_y"] += stats["speed"]
        }
    } else if(unit["direction_y"] == -1 && unit["move_y"] > 1) {
        if(len(attacking) != 0 && attacking["move_y"] < unit["move_y"] - 1) {
            unit["move_y"] -= stats["speed"]
        } else if (len(attacking) == 0 && unit["move_y"] > 1) {
            unit["move_y"] -= stats["speed"]
        }
    }
    if(unit["direction_x"] == 1 && unit["move_x"] < 99) {
        if(len(attacking) != 0 && attacking["move_x"] > unit["move_x"] + 1) {
            unit["move_x"] += stats["speed"]
        } else if (len(attacking) == 0 && unit["move_x"] < 99) {
            unit["move_x"] += stats["speed"]
        }
    } else if(unit["direction_x"] == -1 && unit["move_x"] > 1) {
        if(len(attacking) != 0 && attacking["move_x"] < unit["move_x"] - 1) {
            unit["move_x"] -= stats["speed"]
        } else if (len(attacking) == 0 && unit["move_x"] > 1) {
            unit["move_x"] -= stats["speed"]
        }
    }
}

func return_columns_object(x int, y int, able_to_move int, army_id int, team int) map[string]int {
    return map[string]int{
        "x":  x,
        "y":  y,
        "able_to_move":  able_to_move,
        "move_y":        0,
        "move_x":        50,
        "direction_y":   0,
        "direction_x":   0,
        "target_y":      -999999999,
        "target_x":      -999999999,
        "stopped":       0,
        "attacking_y":   -999999999,
        "attacking_x":   -999999999,
        "killed":        0,
        "facedirection": 0,
        "frame":         16,
        "status":        0,
        "old_move_y":    0,
        "old_move_x":    0,
        "changed":       0,
        "id":            army_id,
        "team":          team,
        "hp":            100,
    }
}

func set_attacking_and_target() {
    elem, ok := group[unit["team"]][unit["attacking_y"]][unit["attacking_x"]]
    if ok {
      attacking = elem
    }
    elem1, ok1 := group[unit["team"]][unit["target_y"]][unit["target_x"]]
    if ok1 {
      target = elem1
    }
}


func iterate_y(y int, units int) int {
    if units == 1 {
        elem, ok := group[-1][y]
        _ = elem
        if ok { return y } else {
            var direction int = 0
            for i := 1; i < 300; i++ {
                elem1, ok1 := group[-1][y+1]
                _ = elem1
                if ok1{ 
                    direction = i
                    break
                } else {
                    elem2, ok2 := group[-1][y-1]
                    _ = elem2
                    if ok2 { 
                      direction = -i
                      break
                    }
                }
            }
            if direction > 0 {
                var last int = 0
                for i := direction; i < 300; i++ {
                    elem3, ok3 := group[-1][y+1]
                    _ = elem3
                    if ok3 { 
                        last = i
                    } else {
                        return y + last
                    }
                }
            } else if direction < 0 {
                var last int = 0
                for i := direction; i > -300; i-- {
                    elem4, ok4 := group[-1][y+1]
                    _ = elem4
                    if ok4 { 
                        last = i
                    } else {
                        return y + last
                    }
                }
            }
            return y
        }
    } else {
        elem5, ok5 := group[1][y]
        _ = elem5
        if ok5 { return y } else {
            var direction int = 0
            for i := 1; i < 300; i++ {
                elem6, ok6 := group[1][y+1]
                _ = elem6
                if ok6{ 
                    direction = i
                    break
                } else {
                    elem7, ok7 := group[1][y-1]
                    _ = elem7
                    if ok7 { 
                      direction = -i
                      break
                    }
                }
            }
            if direction > 0 {
                var last int = 0
                for i := direction; i < 300; i++ {
                    elem8, ok8 := group[1][y+1]
                    _ = elem8
                    if ok8 { 
                        last = i
                    } else {
                        return y + last
                    }
                }
            } else if direction < 0 {
                var last int = 0
                for i := direction; i > -300; i-- {
                    elem9, ok9 := group[1][y+1]
                    _ = elem9
                    if ok9 { 
                        last = i
                    } else {
                        return y + last
                    }
                }
            }
            return y
        }
    }
}

func setAllyEnemy() {
    if unit["team"] == 1 {
        unit["ally"] = 1;
        unit["enemy"] = -1;
    } else {
        unit["ally"] = -1;
        unit["enemy"] = 1;
    }
}

func try_moving() {
    unit["stopped"] = 0
    attacking = make(map[string]int)
    if (len(target) != 0) {
        move_forward()
        if(target["y"] > unit["y"]) {
            if (check_around_for_allies(1)) {
                move_y_axis(1);
            }
        } else if(target["y"] < unit["y"]) {
            if (check_around_for_allies(-1)) {
                move_y_axis(-1);
            }
        }
    }
}

func move_y_axis(direction int) {
    unit["move_y"] += stats["speed"] * direction
    unit["direction_y"] = direction
    if unit["move_y"] > 99 || unit["move_y"] < 1 {
        unit["y"] += direction
        unit["move_y"] = unit["move_y"] - (100 * direction)
    }
}

func check_around_for_allies(direction int) bool {
    var new_y int = unit["y"] + direction
    if len(group[unit["ally"]][new_y][unit["x"]]) != 0 { return false }
    return true
}


func move_forward() {
    if target["x"] > unit["x"] {
        if len(group[unit["ally"]][unit["y"]][unit["x"]+1]) == 0 {
            start_movements(1, 0, 2, 1)
        } else {
            if unit["facedirection"] == 5 {
                unit["frame"] = 13
            } else {
                unit["frame"] = 3
            }
        }
    } else if target["x"] < unit["x"]  {
        if len(group[unit["ally"]][unit["y"]][unit["x"]-1]) == 0 {
            if unit["status"] == 0 {
                if len(group[unit["ally"]][unit["y"]][unit["x"]-2]) == 0 {
                    start_movements(-1, 6, 4, 5)
                }
            } else {
                start_movements(-1, 6, 4, 5)
            }
        } else {
            if unit["facedirection"] == 5 {
                unit["frame"] = 13
            } else {
                unit["frame"] = 3
            }
        }
    } else {
        if target["y"] > unit["y"] {
            unit["facedirection"] = 7
        } else if target["y"] < unit["y"] {
            unit["facedirection"] = 3
        }
    }
}

func start_movements(axis int, face1 int, face2 int, face3 int) {
    unit["frame"]--
    move_x_axis(axis)
    if (target["y"] > unit["y"]) {
        unit["facedirection"] = face1;
    } else if (target["y"] < unit["y"]) {
        unit["facedirection"] = face2;
    } else {
        unit["facedirection"] = face3;
    }
}


func move_x_axis(direction int) {
    unit["move_x"] += stats["speed"] * direction;
    unit["direction_x"] = direction;
    if unit["move_x"] > 99 || unit["move_x"] < 1 {
        unit["x"] += direction;
        unit["move_x"] = unit["move_x"] - (100 * direction);
    }
}


func CalculateFight(msg *workers.Msg) {
  set_default_stats()
}

func set_default_stats() {
  initiate()
  calculate_units()
  fmt.Println("done")
}